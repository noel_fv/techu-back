'use strict'

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const api = require('./api/routes');
const morgan = require('morgan');
const logger = require('./api/utils/logger');
require('./config');

const app = express();

app.use(validator());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan(process.env.NODE_ENV));
app.use('/api', api);


mongoose.connect(process.env.URLDB,{useNewUrlParser: true ,useCreateIndex:true}, (err, res) => {

    if (err) throw err;

    logger.info('Entorno de despliegue ' + process.env.NODE_ENV);
    logger.info('Conectandose a la base de datos ' +process.env.URLDB);
    console.log('Base de datos ONLINE');

});

module.exports = app;
