'use strict'

const express = require('express');
const clientesController = require('../controllers/clientes');
const cuentasController = require('../controllers/cuentas');
const api = express.Router();

const cors = require('cors');
api.use(cors()); //Alow all cors request

//clientes'
api.get('/clientes', clientesController.getAll);
api.get('/clientes/:id', clientesController.findByID);
api.get('/clientes/numeroDocumento/:numeroDocumento', clientesController.findClientByNumberDoc);
api.post('/clientes', clientesController.save);
 api.put('/clientes/:id', clientesController.update);
 api.delete('/clientes/:id', clientesController.remove);
api.post('/clientes/login', clientesController.login);

//cuenta
api.get('/cuentas', cuentasController.getAll);
api.post('/cuentas',  cuentasController.save);
api.post('/cuentas/movimientos',  cuentasController.insertMovimients);
api.put('/cuentas/:id', cuentasController.update);
api.delete('/cuentas/:id', cuentasController.remove);
api.get('/cuentas/numeroCuenta/:numeroCuenta',  cuentasController.findByNumberAccount);
api.get('/cuentas/numeroDocumento/:numeroDocumento',  cuentasController.findNumberAccountByClient);

module.exports = api;
