const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let clienteSchema = new Schema({
  tipoDocumento: {
      type: String,
      required: [true, 'El tipo documento es necesario']
  },
  numeroDocumento: {
      type: String,
      unique: true,
      required: [true, 'El numeroDocumento es necesario']
  },
  nombres: {
    type: String,
    required: [true, 'El nombre es necesario']
  },
  apellidos: {
    type: String,
    required: [true, 'El apellido es necesario']
  },
  password: {
      type: String,
      required: [true, 'La contraseña es obligatoria']
  },
  fechaCreacion: {
      type: Date,
      default: Date.now()
  },
  estado: {
      type: Boolean,
      default: true
  }
});


clienteSchema.methods.toJSON = function() {

  let user = this;
  let userObject = user.toObject();
  delete userObject.password;

  return userObject;
}


clienteSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Cliente', clienteSchema);
