const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Cliente = require('../models/clientes');

let Schema = mongoose.Schema;

let movimientoSchema = new Schema({
   tipoMovimiento: {
     type: String,
     enum : ['ABONO','CARGO']
   },
   importe: {
     type: Schema.Types.Decimal128,
      default:0.0
   },
   fecha: {
    type: Date,
    default: Date.now()
   }
  });

let cuentaSchema = new Schema({
  tipoCuenta: {
      type: String,
      required: [true, 'El tipo tipoCuenta es necesario']
  },
  numeroCuenta: {
      type: String,
      unique: true,
      required: [true, 'El numeroCuenta es necesario']
  },
  moneda: {
    type: String,
    enum : ['PEN','DOL']
  },
  saldo: {
    type: Schema.Types.Decimal128,
    default:0.0
  },
  _idCliente: {
      //type: String
      //type: Schema.Types.ObjectId
      type: Schema.ObjectId, ref: 'Cliente'
  },
  movimientos: [
        movimientoSchema
  ],
  estado: {
      type: Boolean,
      default: true
  },
  fecha: {
    type: Date,
    default: Date.now()
   }
});


cuentaSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Cuenta', cuentaSchema);
