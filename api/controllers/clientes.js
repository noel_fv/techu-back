'use strict'

const bcrypt = require('bcrypt');
const _ = require('underscore');
const Cliente = require('../models/clientes');
const logger = require('../utils/logger');

async function findByID(req, res) {


  Cliente.findById(req.params.id)
      .exec((err, clienteDB) => {

          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              });
          }

      if(clienteDB === null || clienteDB === 'undefined'){
                
            return res.status(404).json({
                ok: false,
                message : 'Cliente no encontrado'
            });

        }

          res.json({
            ok: true,
            clienteDB
        });

      });
};


async function save(req,res) {

  let body = req.body;

  let cliente = new Cliente({
    tipoDocumento: body.tipoDocumento,
    numeroDocumento: body.numeroDocumento,
    nombres: body.nombres,
    apellidos: body.apellidos,
    password: bcrypt.hashSync(body.password, 10)
  }); 

  await cliente.save((err, clienteDB) => {
      if (err) {
          return res.status(400).json({
              ok: false,
              err
          });
      }
      res.json({
          ok: true,
          usuario: clienteDB
      });
  });
};


async function update(req, res) {

  let id = req.params.id;
  let body = { "$set": req.body };

  await Cliente.findOneAndUpdate(id, body, { new: true, runValidators: true }, (err, clienteDB) => {

      if (err) {
          return res.status(400).json({
              ok: false,
              err
          });
      }

      res.json({
          ok: true,
          usuario: clienteDB
      });

  })

};

async function remove(req, res) {


  let id = req.params.id;

  let cambiaEstado = {
      estado: false
  };

  Cliente.findOneAndUpdate(id, cambiaEstado, { new: true }, (err, clienteBorrado) => {

      if (err) {
          return res.status(400).json({
              ok: false,
              err
          });
      };

      if (!clienteBorrado) {
          return res.status(400).json({
              ok: false,
              err: {
                  message: 'Usuario no encontrado'
              }
          });
      }

      res.json({
          ok: true,
          usuario: clienteBorrado
      });

  });

};

async function findClientByNumberDoc(req, res) {

  let filter = {
    numeroDocumento : req.params.numeroDocumento,
    estado:true
  };

 await Cliente.findOne(filter,'nombres apellidos numeroDocumento')
      .exec((err, clienteDB) => {

          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              });
          }

          if(clienteDB === null || clienteDB === 'undefined'){
            return res.status(404).json({
                ok: false,
                message : 'Cliente no encontrado'
            });
            }

          res.json({
            ok: true,
            clienteDB
        });

      });
};

async function login(req, res) {

    let body = req.body;

    let filter = {
      numeroDocumento : body.numeroDocumento,
      estado:true
    };
  
   await Cliente.findOne(filter)
        .exec((err, clienteDB) => {
  
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
  
            if(clienteDB === null || clienteDB === 'undefined'){
                
                return res.status(404).json({
                    ok: false,
                    message : 'Cliente no encontrado'
                });

            }else{

                if(bcrypt.compareSync(body.password, clienteDB.password)) {
                    res.json({
                        ok: true,
                        message : 'Logeo satisfactorio',
                        cliente : clienteDB,
                        token : 'token'
                    });
                } else {
                    return res.status(404).json({
                        ok: false,
                        message : 'Contraseña incorrecta'
                    });
                }
            }
  
        });
  };
  



async function getAll(req, res) {

  logger.info('Consultando todos los clientes');
  Cliente.find({estado:true})
      .exec((err, clienteDB) => {

          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              });
          }

          res.json({
            ok: true,
            clienteDB
        });

      });
};


module.exports = {
    save,
    update,
    remove,
    findByID,
    findClientByNumberDoc,
    getAll,
    login
};


/* let desde = req.query.desde || 0;
desde = Number(desde);

let limite = req.query.limite || 5;
limite = Number(limite);

Usuario.find({ estado: true }, 'nombre email role estado google img')
    .skip(desde)
    .limit(limite)
    .exec((err, usuarios) => { */