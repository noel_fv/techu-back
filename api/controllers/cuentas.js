'use strict'

const Cuenta = require('../models/cuentas');
const Cliente = require('../models/clientes');
//https://github.com/Automattic/mongoose/issues/4874

async function save(req,res) {

  let body = req.body;

  let filter = {
    numeroDocumento : body.numeroDocumento,
    estado:true
  };

  await Cliente.findOne(filter)
      .exec((err, clienteDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(clienteDB === null || clienteDB === 'undefined'){
            return res.status(404).json({
                ok: false,
                message : 'Cliente no encontrado'
            });
            }

      let cuenta = new Cuenta({
        tipoCuenta: body.tipoCuenta,
        numeroCuenta: body.numeroCuenta,
        moneda: body.moneda,
        saldo: body.saldo,
        _idCliente:clienteDB._id
      });

       cuenta.save((err, cuentaDB) => {
          if (err) {
              return res.status(400).json({
                  ok: false,
                  err
              });
          }
          res.json({
              ok: true,
              cuenta: cuentaDB
          });
      });
    });
};

async function update(req, res) {

  let id = req.params.id;
  let body = { "$set": req.body };

  await Cuenta.findOneAndUpdate(id, body, { new: true, runValidators: true }, (err, cuentaDB) => {

      if (err) {
          return res.status(400).json({
              ok: false,
              err
          });
      }

      res.json({
          ok: true,
          cuenta: cuentaDB
      });

  })

}; 

async function remove(req, res) {


    let id = req.params.id;
  
    let cambiaEstado = {
        estado: false
    };
  
    Cuenta.findOneAndUpdate(id, cambiaEstado, { new: true }, (err, cuentaBorrado) => {
  
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        };
  
        if (!cuentaBorrado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Cuenta no encontrado'
                }
            });
        }
  
        res.json({
            ok: true,
            cuenta: cuentaBorrado
        });
  
    });
};

async function findByNumberAccount(req, res) {

let filter = {
    numeroCuenta : req.params.numeroCuenta,
    estado:true
};

await Cuenta.findOne(filter)
    .exec((err, cuentaDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            cuenta : cuentaDB
        });

    });
};

async function findNumberAccountByClient(req , res){

let filter = {
    numeroDocumento : req.params.numeroDocumento,
    estado:true
    };

    await Cliente.findOne(filter,'nombres')
        .exec((err, clienteDB) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            if(clienteDB === null || clienteDB === 'undefined'){
            return res.status(404).json({
                ok: false,
                message : 'Cliente no encontrado'
            });
            }

    let filter = {
    _idCliente : clienteDB._id,
    estado:true
    };
        
    Cuenta.find(filter)
                .exec((err, cuentaDB) => {
        
                    if (err) {
                        return res.status(400).json({
                            ok: false,
                            err
                        });
                    }
        
                    res.json({
                    ok: true,
                    cuenta : cuentaDB
                });
        
                });
            
        });
};

async function insertMovimients(req,res) {

let saldoTemp=0.0;
let body = req.body;

let filter = {
    numeroCuenta : body.numeroCuenta,
    estado:true
};

await Cuenta.findOne(filter)
    .exec((err, cuentaDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(cuentaDB === null || cuentaDB === 'undefined'){
            return res.status(404).json({
                ok: false,
                message : 'Cuenta no encontrado'
            });
        }

        if(body.tipoMovimiento==='ABONO'){
            saldoTemp= parseFloat(cuentaDB.saldo)+parseFloat(body.importe);  
        }else{
            if(parseFloat(body.importe)>parseFloat(cuentaDB.saldo)){
                return res.status(400).json({
                    ok: false,
                    message: 'Saldo insufuciente!'
                });
            }
            saldoTemp= parseFloat(cuentaDB.saldo)-parseFloat(body.importe);  
        }
    
    /* Cuenta.findOneAndUpdate( */
    Cuenta.updateOne(
        {numeroCuenta:body.numeroCuenta},
        {   
            $set : {saldo: saldoTemp.toFixed(2)},
            $push: {'movimientos': { importe: body.importe ,  tipoMovimiento: body.tipoMovimiento }}
        }
        ,(err, cuentaNewDB) => {
                    
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                cuenta: cuentaNewDB
            });
    });





/*      Cuenta.findOneAndUpdate(cuentaDB._id,{"$set": {
            saldo: saldoTemp.toFixed(2),
            movimientos:[{ importe: body.importe }, { tipoMovimiento: body.tipoMovimiento }]
        }
    },(err, cuentaNewDB) => {
                
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cuenta: cuentaNewDB
            });
    }); 
    
    
    
    Cuenta.update(
        {numeroCuenta:body.numeroCuenta},
        {   
            $push:{movimientos:
                {
                    $each:[{ importe: body.importe ,  tipoMovimiento: body.tipoMovimiento }]
                }
            } 
            
        },(err, cuentaNewDB) => {
                    
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cuenta: cuentaNewDB
            });
    });

    */
    

    });
};


 async function getAll(req, res) {

        Cuenta.find({estado:true})
            .exec((err, cuentaDB) => {
      
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }

                if(cuentaDB === null || cuentaDB === 'undefined'){
                    return res.status(404).json({
                        ok: false,
                        message : 'Cuenta no encontrado'
                    });
                }
      
                res.json({
                  ok: true,
                  cuentaDB
              });
      
            });
      };

module.exports = {
    save,
    update,
    remove,
    findByNumberAccount,
    findNumberAccountByClient,
    insertMovimients,
getAll
};
