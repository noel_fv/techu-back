
'use strict';

const { createLogger, format, transports } = require('winston');
const path = require('path');

const logger = createLogger({
  level: process.env.LOG_LEVEL,
  /* format: format.simple(), 
  format: format.combine(format.colorize(), format.simple()),*/
  format: format.combine(
    format.label({ label: path.basename(process.mainModule.filename) }),
    format.colorize(),
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
   /*  format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`) */
    format.printf(info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)
  ),
  // You can also comment out the line above and uncomment the line below for JSON format
  // format: format.json(),
  transports: [new transports.Console()]
});

logger.info('Logger activado');


module.exports = logger