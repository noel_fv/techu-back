# image base
FROM node:latest

#directorio del app
WORKDIR /app

#copiado de archivos
ADD . /app

#dependencies
RUN apt-get update
RUN npm install

#Variable de entorno
ENV NODE_ENV production

#puerto a exponer
EXPOSE 3000

#comando
CMD ["npm","start"]
