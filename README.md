# Título del Proyecto

BANKITO BBVA - TECHU

## Comenzando 🚀

_Este componente se encarga de la contruccion del API del proyecto, el cual se encarga de resolver todas las peticiones enviadas al API para su tratamiento o recuperacion de datos de una base de datos MongoDB_

Mira **Deployment** para conocer como desplegar el proyecto.


### Instalación 🔧

_Para la intalacion del componente por favor seguir los siguientes pasos:_

_Si no tiene git instalado (ir  https://git-scm.com/downloads)_

```
git clone https://noel_fv@bitbucket.org/noel_fv/techu-back.git

```

_Luego ingresar a la carpeta techu-back_

```
cd techu-back

```

_Descargar todas las depencias del proyecto_

```
npm install

```

_Desplegar el proyecto_

```
npm start

```

_Abrir la url del despliegue en un navegador para su validacion_

## Ejecutando las pruebas ⚙️

_Explica como ejecutar las pruebas automatizadas para este sistema_

### Analice las pruebas end-to-end 🔩

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

### Y las pruebas de estilo de codificación ⌨️

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

## Deployment 📦

_El despliegue del componente techu-back lo puedes realizar en un entorno cloud para nuestras pruebas lo realizaremos en un servidor Heroku_



## Construido con 🛠️

_Herramientas de desarrolo_

* [NodeJS](https://nodejs.org/es/download/) - Framework para la construccion del componente
* [MongoDB](https://www.mongodb.com/download-center) - Para almacenamiento de datos
* [Visual studio code](https://code.visualstudio.com/download) - IDE de desarrollo
* [Postman](https://www.getpostman.com/downloads/) - Para las pruebas
* [Docker](https://www.docker.com/get-started) - Para la generacion de imagenes


## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/villanuevand/xxxxxx) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Noel Flores** - *Trabajo Inicial* - [noel_fv](https://bitbucket.org/noel_fv/)
* **Diana Uriol** - *Trabajo Inicial* - [diana](#diana)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto.

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 a alguien del equipo.
* Da las gracias públicamente 🤓.
* etc.
