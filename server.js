'use strict'

const app = require('./app');

app.listen(process.env.PORT, process.env.IP,() => {
  console.log( 'API REST SERVER RUNNING ... PORT= ' + process.env.PORT);
});
