// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  IP
// ============================
process.env.IP = process.env.IP || '0.0.0.0'


// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ============================
//  Nivel de log
// ============================
process.env.LOG_LEVEL = process.env.LOG_LEVEL || 'debug'

// ============================
//  Base de datos
// ============================
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/techudb';
    //urlDB = 'mongodb+srv://test:test@cluster0-h99at.mongodb.net/techudb';
  //  urlDB = 'mongodb://admin123:admin123@ds229552.mlab.com:29552/wflores';
} else {
    //urlDB = 'mongodb+srv://test:test@cluster0-h99at.mongodb.net/techudb';
    urlDB = 'mongodb://admin123:admin123@ds229552.mlab.com:29552/wflores';
}
process.env.URLDB = urlDB;